import sys, os, threading
import time
import argparse
import csv, json

from PySide2.QtCore import QObject, Signal, Property, QUrl
from PySide2.QtGui import *
from PySide2.QtQml import *
from PySide2 import *

from pythonosc import osc_message_builder
from pythonosc import udp_client


from pythonosc import osc_message_builder
from pythonosc import udp_client

from pythonosc import dispatcher
from pythonosc import osc_server

from random import shuffle
from collections import deque

from SerialListener import *

SEND_PORT   = 8010

FIXTURE_ADDR = "/fixtures/Fixture-{}/visible"
SCENE_HOME_ADDR = "/cues/Bank-1/scenes/by_cell/col_1"
SCENE_GAME_ADDR = "/cues/Bank-1/scenes/by_cell/col_2"

def sendOSC( addr, arg=None ) :
    osc_client = udp_client.UDPClient("127.0.0.1", SEND_PORT)
    msg = osc_message_builder.OscMessageBuilder( address = addr )
    # if arg :
    msg.add_arg( arg )
    print( "arg", addr, arg )
    msg = msg.build()
    osc_client.send( msg )


class ButtonListener( BaseSerialListener ) :
    def __init__( self, app, port ) :
        self.app = app
        super( ButtonListener, self ).__init__( port )

    def getReadBytes( self ):
        return 2

    def onDataReceived( self, data ) :
        resp = struct.unpack( "<bb", data )
        if resp[1] == 0 :
            button = resp[0]
            print( "Button", button, "pressed")
            if( button == self.app.currentButton ):
                self.app.buttonPressed.emit( int(button) )
                self.app.next()


class BuzzerListener( BaseSerialListener ) :
    def __init__( self, app, port ) :
        self.app = app
        super( BuzzerListener, self).__init__( port )

    def getReadBytes( self ):
        return 2

    def onDataReceived( self, data ) :
        resp = struct.unpack( "<bb", data )
        if resp[1] == 0 :
            if self.app.gameState == "home":
                self.app.startGame()


class Application( QObject ):
    userRegistered = Signal(str)
    buttonPressed = Signal(int)
    gameStarted = Signal()
    gameStopped = Signal()
    currentButton = 0
    gameState = "home"

    def __init__( self ):
        QObject.__init__( self )
        self.app = QGuiApplication( sys.argv )
        engine = QQmlApplicationEngine()
        engine.quit.connect( self.quit )
        self.app.lastWindowClosed.connect( self.quit )

        appDirPath = os.path.dirname( os.path.abspath( __file__ ) )
        rootContext = engine.rootContext()
        rootContext.setContextProperty( 'controller', self )
        rootContext.setContextProperty( 'appPath', appDirPath.replace( "\\", "/" ) + "/" )
        print( 'appPath', appDirPath.replace( "\\", "/" ) + "/" )

        parser = argparse.ArgumentParser()
        parser.add_argument( "-f", action='store_true', help="fullscreen" )
        parser.add_argument( "-v", action='store_true', help="verbose" )
        parser.add_argument( "--timeout", type=int, default="30", help="Timeout in seconds" )
        parser.add_argument( "--serPorts", type=str, default="3", help="id of arduino buzzer,buttons " )
        
        args = parser.parse_args()
        rootContext.setContextProperty('timeout', args.timeout  )    
        rootContext.setContextProperty('verbose', args.v  )    
        
        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.set_default_handler( self.def_osc_handler )
        server = osc_server.ThreadingOSCUDPServer(
            ("0.0.0.0", 3000 ), self.dispatcher )
        print( "Serving on {}".format( server.server_address ) )
        threading.Thread( target=server.serve_forever, daemon=True ).start()

        arduinos = args.serPorts.split(',')
        self.buzzerListener = BuzzerListener( self, arduinos[0] )
        self.buttonListener = ButtonListener( self, arduinos[1] )
        windowComp = QQmlComponent( engine, QUrl.fromLocalFile( 'qml/main.qml' ) )
        self.window = windowComp.create()
        if not self.window:
            print( "Unable to load qml: {0}".format( windowComp.errorString() ) )
            return
        iconPath = os.path.join( appDirPath, os.path.normcase( './qml/images/icon.ico' ) )
        self.window.setIcon( QIcon( iconPath ) )

        if args.f:
            self.window.showFullScreen()
        else:
            self.window.show()
        self.app.exec_()

    @QtCore.Slot( int, int )
    def setLED( self, num, state ):
        if state == 0 :
            sendOSC(FIXTURE_ADDR.format(num + 1), 0)
            sendOSC(FIXTURE_ADDR.format(num + 1 + 8), 0)
        if state == 1 :
            sendOSC(FIXTURE_ADDR.format(num + 1), 1)
            sendOSC(FIXTURE_ADDR.format(num + 1 + 8), 0)
        if state == 2 :
            sendOSC(FIXTURE_ADDR.format(num + 1), 0)
            sendOSC(FIXTURE_ADDR.format(num + 1 + 8), 1)
    
    @QtCore.Slot(int)
    def setAllLEDs( self, state ):
        for i in range( 0, 8 ) :
            self.setLED(i, state)

    def def_osc_handler( self, addr, *args ):
      print( "recvd", addr, *args )
      args = list( args )
      if addr == "/register/user" :
          self.userRegistered.emit(args[0])

    def startGame(self):
        self.q = deque([i for i in range(8)])
        shuffle(self.q)
        sendOSC(SCENE_GAME_ADDR)
        self.setAllLEDs(0)
        self.next()
        self.gameStarted.emit()

    def home(self):
        self.setAllLEDs(0)
        sendOSC(SCENE_HOME_ADDR)

    def next(self):
        self.setLED(self.currentButton, 2)
        if len(self.q):
            self.currentButton = self.q.pop()
            print("curButton", self.currentButton)
            print("q", self.q)
            self.setLED(self.currentButton, 1)
        else:
            self.gameStopped.emit()
            self.home()


    def cleanup( self ):
        self.buttonListener.stop()
        self.buzzerListener.stop()

    def quit( self ):
        self.cleanup()
        self.app.quit()


if __name__ == "__main__":
    app = Application()
