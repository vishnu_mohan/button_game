#define EI_ARDUINO_INTERRUPTED_PIN
#include <EnableInterrupt.h>

int BTNS[] = { 2, 3, 4, 5, 7, 8, 9, 10 };
unsigned long lastInterrupt = 0;
int lastBouncePin = 0;
struct __attribute__((__packed__)) serialParams {
  char id;
  boolean state;
};

void setup() {
  for ( int i = 0; i < 8; i++) {
    pinMode( BTNS[ i ], INPUT_PULLUP );
    enableInterrupt( BTNS[ i ], btnPressed, CHANGE );
  }
  Serial.begin(9600);
  delay(1000);
}

void btnPressed() {
//  Serial.println(arduinoInterruptedPin);
  unsigned long now = millis();
  if ( ( lastBouncePin == arduinoInterruptedPin ) && ( (now - lastInterrupt) < 100 ) ) {
    lastBouncePin = arduinoInterruptedPin;
    lastInterrupt = now;
    return;
  }
  serialParams p;
  p.id = (char) index(arduinoInterruptedPin);
  p.state = digitalRead( arduinoInterruptedPin );
  if ( p.state == LOW ) {
    lastBouncePin = arduinoInterruptedPin;
    lastInterrupt = now;
    Serial.write( (byte *) &p, 2 );
    Serial.flush();
//        Serial.println(index(arduinoInterruptedPin));
  }
}

int index( int pin ) {
  for ( int i = 0; i < 8; i++ ) {
    if ( BTNS[i] == pin) {
      return i;
    }
  }
  return -1;
}

void loop() {

}
