import struct
import serial.tools.list_ports
import re
from threading import Thread
import time
from queue import Queue

RE_HDWR_ID = re.compile( '10C4:EA60|2341:0001|2341:0043|2A03:0043|2341:0243|2341:0042' )
# teensy '16C0:0483|16C0:0487|16C0:0489|16C0:048A|16C0:0476'

def getSerialPorts():
    ard_ports = []
    for port in serial.tools.list_ports.comports():
        if RE_HDWR_ID.search( port.hwid ):
            print( "Found arduino at port: {}".format( port.device ) )
            ard_ports.append( port.device )
    return ard_ports

class BaseSerialListener( Thread ):
    def __init__( self, port ):
        super( BaseSerialListener, self ).__init__()
        self.quit = False
        self.sendQ = Queue()
        self.port = port
        self.start()
    
    def run( self ):
        while not self.quit:
            port = self.port
            if not port:
                time.sleep( 1.0 )
                continue
            try:
                with serial.Serial( port, 9600, timeout=2 ) as ser:
                    self.onConnected( port )
                    time.sleep( 2 )
                    while not self.quit:
                        if( self.getReadBytes() > 0 ) :
                            data = ser.read( self.getReadBytes() )
                            try:
                                if len(data) >= self.getReadBytes() :
                                    self.onDataReceived( data )
                            except Exception as e:
                                print( e )
                        if( not self.sendQ.empty() ):
                            ser.write( self.sendQ.get() )
            except Exception as e:
                self.onDisconnected( e )
 
    def sendData( self, data ):
        self.sendQ.put( data )

    def onConnected( self, port ):
        print( "Connected to port", port )

    def onDisconnected( self, e ):
        print( "Disconnected", e )

    def getReadBytes( self ):
        pass

    def onDataReceived( self, data ):
        print( "Recieved Data", data )

    def stop( self ):
        self.quit = True
