import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtMultimedia 5.9
import Qt.labs.folderlistmodel 2.14

Window {
    width: 1920
    height: 1080
    id: main
    color: "black"
    flags: Qt.FramelessWindowHint | Qt.Window
    property string state: "home"
    property int curImage: 0
    property variant imgs: [img0, img1, img2, img3, img4, img5, img6, img7]
    property string curUser: ""

    property int totalDuration: 100*1000
    Rectangle {
        id: countdown
        height: parent.height
        width: parent.width / 3

        property int elapsed: totalDuration

        Timer {
            id: countdownTimer
            interval: 9
            running: true
            repeat: true
            onTriggered: {
                parent.elapsed -= interval
                var sec = Math.floor(parent.elapsed / 1000)
                var min = Math.floor(sec / 60)
                time.text = ("0" + min).slice(
                            -2) + ":" + ("0" + (sec % 60).toFixed(3)).slice(-6)
                if( parent.elapsed <= 0)
                    stopGame()
            }
        }

        FontLoader {
            id: webFont
            source: "font.ttf"
        }

        Text {
            id: time
            anchors.fill: parent
            font.pixelSize: parent.width/10
            color: "#f0b419"
            text: "00:00.000"

            font.family: webFont.name
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            MultiPointTouchArea {
                anchors.fill: parent
                onPressed: {
                    buttonPressed(0)
                }
            }
        }
    }

    Rectangle {
        id: game
        height: parent.height
        width: parent.width / 3
        anchors.left: countdown.right
        color: "black"
        property int score: 0

        Image {
            id: imgBg
            source: "images/bg.png"
            anchors.fill: parent
            focus: true
            visible: main.state === "game"

            Item {
                id: parts
                anchors.fill: parent

                Image {
                    id: img0
                    anchors.fill: parent
                    source: "images/parts/img_0.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 0
                }

                Image {
                    id: img1
                    anchors.fill: parent
                    source: "images/parts/img_1.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 1
                }

                Image {
                    id: img2
                    anchors.fill: parent
                    source: "images/parts/img_2.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 2
                }

                Image {
                    id: img3
                    anchors.fill: parent
                    source: "images/parts/img_3.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 3
                }

                Image {
                    id: img4
                    anchors.fill: parent
                    source: "images/parts/img_4.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 4
                }

                Image {
                    id: img5
                    anchors.fill: parent
                    source: "images/parts/img_5.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 5
                }

                Image {
                    id: img6
                    anchors.fill: parent
                    source: "images/parts/img_6.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 6
                }

                Image {
                    id: img7
                    anchors.fill: parent
                    source: "images/parts/img_7.png"
                    fillMode: Image.PreserveAspectFit
                    visible: game.score > 7
                }
            }
        }

        Image {
            source: "images/home.png"
            visible: main.state === "home"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }

        Image {
            source: "images/" + main.state + ".png"
            visible: main.state === "win" ||  main.state === "lose"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }

        focus: true
        Keys.onPressed: {
            if (event.key == Qt.Key_Enter)
                startGame()
            
            if (event.key == Qt.Key_Space)
                    userRegistered("John Doe")
            
        }
    }

    Rectangle {
        id: user
        height: parent.height
        width: parent.width / 3
        anchors.left: game.right

        Image {
            source: "images/bg_reg.png"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }


        Text {
            anchors.centerIn: parent
            text: "Welcome\n" + curUser
            color: "white"
            font.family: webFont.name
            font.pixelSize: parent.width/15
        }

        Image {
            source: "images/home_reg.png"
            visible: main.state === "home"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }

    Timer {
        id: timeoutTimer
        interval: 10 * 1000
        running: false
        onTriggered: {
            reset()
        }
    }

    function reset() {
        main.state = "home"
        game.score = 0
        countdownTimer.stop()
        countdown.elapsed = totalDuration
        time.text = "00:00.000"

        img0.visible = false
        img1.visible = false
        img2.visible = false
        img3.visible = false
        img4.visible = false
        img5.visible = false
        img6.visible = false
        img7.visible = false
        controller.setAllLEDs(0)
    }

    function startGame() {
        countdownTimer.start()
    }

    function stopGame() {
        countdownTimer.stop()
        timeoutTimer.start()
        main.state = countdown.elapsed > 0 ? "win" : "lose"
        if( main.state === "lose")
            time.text = "00:00.000"
    }

    function buttonPressed(i) {
        imgs[game.score].visible = true
        game.score++

    }

    function userRegistered(name) {
        reset()
        main.state = "game"
        main.curUser = name
    }

    Component.onCompleted: {
        controller.userRegistered.connect(userRegistered)
        controller.buttonPressed.connect(buttonPressed)
        controller.gameStarted.connect(startGame)
        controller.gameStopped.connect(stopGame)
        reset()
    }

    MultiPointTouchArea {
        anchors.fill: parent
        onPressed: {
            buttonPressed(0)
        }
    }
}
