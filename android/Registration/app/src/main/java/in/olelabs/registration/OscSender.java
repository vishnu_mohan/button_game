/**
 * Created by user on 10/25/2014.
 */

package in.olelabs.registration;

import android.util.Log;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortOut;

import java.net.InetAddress;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

//
//import com.illposed.osc.OSCMessage;
//import com.illposed.osc.OSCPortOut;


class OscSender implements Runnable {
    private static OscSender sInstance = null;
    
    private String mPrevIp = "";
    private OSCPortOut mOscPort = null;
    private static final int kOscPort = 3000;

    private static class MsgTuple {
        public String ip;
        public OSCMessage msg;

        public MsgTuple( String ip, OSCMessage msg ) {
            this.ip = ip;
            this.msg = msg;
        }
    }

    private BlockingQueue<MsgTuple> mMessageQueue = new LinkedBlockingQueue<MsgTuple>();

    protected OscSender() {
        new Thread( this ).start();
    }

    public static OscSender getInstance() {
        if( sInstance == null ) {
            sInstance = new OscSender();
        }
        return sInstance;
    }

//    public void sendValues( String ipAddr, String oscAddr, int id, Float... vals ) {
//        OSCMessage msg = new OSCMessage( oscAddr ,);
//        ArrayList<Float> vals = new ArrayList<>();
//        msg.addArgument( id );
//        for( int i = 0; i < vals.length; i++ )
//            msg.addArgument( vals[ i ] );
//        mMessageQueue.add( new MsgTuple( ipAddr, msg ) );
//    }

    public void sendValues( String ipAddr, String oscAddr, String id ) {
        OSCMessage msg = new OSCMessage( oscAddr );
//        ArrayList<Float> vals = new ArrayList<>();
        msg.addArgument( id );
        mMessageQueue.add( new MsgTuple( ipAddr, msg ) );
    }

    public OSCPortOut getOscPort( String ip ) {
        if( mPrevIp != null && mPrevIp.equals( ip ) )
            return mOscPort;

        try {
            InetAddress inetAddr = InetAddress.getByName( ip );
            OSCPortOut port = new OSCPortOut( inetAddr, kOscPort );
            mPrevIp = ip;
            mOscPort = port;
            return port;
        } catch( Exception ex ) {
            Log.d("OscSender", ex.getMessage());
        }

        return null;
    }

    @Override
    public void run() {
        Log.d("OscSender", "Started Thread");
        while( true ) {
            MsgTuple tuple;
            try {
                tuple = mMessageQueue.take();
            } catch ( InterruptedException e ) {
                continue;
            }
            OSCPortOut port = getOscPort( tuple.ip );
            try {
                port.send( tuple.msg );
            } catch ( Exception e ) {
                Log.d( "OscSender", e.getMessage() );
            }
        }
    }
}
