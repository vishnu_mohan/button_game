package in.olelabs.registration;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        Bundle extras = getIntent().getExtras();
        final String name = extras.getString("name");
        final String mobile = extras.getString("mobile");
        final String email = extras.getString("email");

        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String score = ((EditText) findViewById(R.id.etScore)).getText().toString();
                try {
                    writeCSV("RegistrationData.csv",name,mobile,email,score);
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(ScoreActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    void writeCSV( String fileName, String... data) throws IOException {
        String baseDir = getExternalFilesDir(null).getAbsolutePath();
        String filePath = baseDir + File.separator + fileName;
        Log.d("ScoreActivity", "writeCSV: "+filePath);
        File f = new File(filePath);
        CSVWriter writer;
        if(f.exists()&&!f.isDirectory())
        {
            FileWriter mFileWriter = new FileWriter(filePath, true);
            writer = new CSVWriter(mFileWriter);
        }
        else
        {
            writer = new CSVWriter(new FileWriter(filePath));
        }

        writer.writeNext(data);

        writer.close();
    }
}
