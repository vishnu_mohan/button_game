package in.olelabs.registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;

public class GreetingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                ((TextView) findViewById(R.id.textView)).setText(""+millisUntilFinished / 1000);
            }

            public void onFinish() {
                Intent intent = new Intent(GreetingActivity.this, ScoreActivity.class);
                intent.putExtras(GreetingActivity.this.getIntent());
                startActivity(intent);
                finish();
            }
        }.start();

    }
}
