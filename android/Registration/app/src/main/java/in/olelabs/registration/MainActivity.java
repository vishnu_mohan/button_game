package in.olelabs.registration;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.net.URLEncoder;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MainActivity extends AppCompatActivity {

    final String username = "iqoo_registration@otherlogic.in";
    final String password = "oleindia2020";

    String otp = "";
    String SMS_TEMPLATE = "Please find below the OTP for your registration. Do share this with our brand representatives on site to complete the process. OTP: %s";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = settings.edit();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Server IP");
        alert.setMessage("Enter Server IP");

        final EditText input = new EditText(this);
        alert.setView(input);
        input.setText("192.168.");
        input.setInputType(InputType.TYPE_CLASS_PHONE);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                editor.putString("ip", input.getText().toString());
                editor.commit();
            }
        });

        alert.show();

        findViewById(R.id.btnSubmit).setEnabled(false);

        findViewById(R.id.btnOTP).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = String.format("%04d", new Random().nextInt(10000));
                String email = ((EditText) findViewById(R.id.etEmail)).getText().toString();
                String mob = ((EditText) findViewById(R.id.etMobile)).getText().toString();
                String name = ((EditText) findViewById(R.id.etName)).getText().toString();

                String msg = String.format(SMS_TEMPLATE, otp);
                if (!email.isEmpty()) {
                    AsyncTaskRunner runner = new AsyncTaskRunner();
                    runner.execute(email, name, otp);
                }

                if (!mob.isEmpty())
                    sendSMS(mob, msg);

            }
        });

        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( ((EditText) findViewById(R.id.etOTP)).getText().toString().equals(otp) ) {
                    String email = ((EditText) findViewById(R.id.etEmail)).getText().toString();
                    String mob = ((EditText) findViewById(R.id.etMobile)).getText().toString();
                    String name = ((EditText) findViewById(R.id.etName)).getText().toString();
                    Log.d("MainActivity", "onClick: ");
                    Intent intent = new Intent(MainActivity.this, GreetingActivity.class);
                    intent.putExtra("email", email);
                    intent.putExtra("mobile", mob);
                    intent.putExtra("name", name);
                    startActivity(intent);

                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                    String ip = settings.getString("ip", "");
                    OscSender.getInstance().sendValues(ip, "/register/user", name);
                    ((EditText) findViewById(R.id.etEmail)).setText("");
                    ((EditText) findViewById(R.id.etMobile)).setText("");
                    ((EditText) findViewById(R.id.etName)).setText("");
                    ((EditText) findViewById(R.id.etOTP)).setText("");
                }
            }
        });
    }

    public boolean isOnline() {

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    void    sendEmail(String to, String msg) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");


        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("IQOO: Registration OTP");
            message.setText(msg);
            Transport.send(message);
            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sending..."); // Calls onProgressUpdate()
            String body = "Dear " + params[1] + ",\n" +
                    "\tThank you for participating in the IQOO activation @ Cyberhub, Gurugram\n" +
                    "\tPlease find below your OTP for registration. Do share this with our brand representative on site to complete the process\n" +
                    "OTP: " + params[2];
            sendEmail(params[0], body);
            return "";
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.btnSubmit).setEnabled(true);
                }
            });

        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Sending OTP",
                    "Please wait.");
        }


        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

    void sendSMS(String mob, String msg) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=1ZPyMqBl2gX&MobileNo=91" + mob
                + "&SenderID=IQOOIN&Message=" + URLEncoder.encode(msg) + "&ServiceName=TEMPLATE_BASED";
        Log.d("Request", "url: " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        findViewById(R.id.btnSubmit).setEnabled(true);
                        Log.d("Response", "onResponse: " + response);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("HTTP", "onErrorResponse: "+error.getMessage() );
                Toast.makeText(MainActivity.this, "SMS Error", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.btnSubmit).setEnabled(false);
    }
}